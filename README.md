# Database initialisation

The following variables can be used to create a database on startup:
* `POSTGRES_DB`, name of the database.
* `POSTGRES_USER`, name for a custom user account.
* `POSTGRES_PASSWORD`, password for the custom user account.

In addition, more logic can be applied by mounting `.sql` or `.sh` files into `/docker-entrypoint-initdb.d/`.

# Other variables

* `PGDATA`, location of the postgres data directory.

## Configuration
* `PG_SHARED_BUFFERS`, "shared_buffers"
* `PG_EFFECTIVE_CACHE_SIZE`, "effective_cache_size"
* `PG_CHECKPOINT_COMPLETION_TARGET`, "checkpoint_completion_target"
* `PG_MAINTENANCE_WORK_MEM`, "maintenance_work_mem"
* `PG_WAL_BUFFERS`, "wal_buffers"
* `PG_DEFAULT_STATISTICS_TARGET`, "default_statistics_target"
* `PG_RANDOM_PAGE_COST`, "random_page_cost"
* `PG_EFFECTIVE_IO_CONCURRENCY`, "effective_io_concurrency"
* `PG_WORK_MEM`, "work_mem"
* `PG_MIN_WAL_SIZE`, "min_wal_size"
* `PG_MAX_WAL_SIZE`, "max_wal_size"
* `PG_MAX_WORKER_PROCESSES`, "max_worker_processes"
* `PG_PARALLEL_WORKERS_PER_GATHER`, "max_parallel_workers_per_gather"

