#!/bin/bash
BASE_DIR="${TEST_SCRIPTS_DIR:?err}"
LOG_FILE="/init/init.log"
nohup bash "${BASE_DIR}/test-loop.sh" >"${LOG_FILE}" 2>&1 &
