#!/bin/bash
BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
while  [ ! -f  "/test/done" ]; do
	nc -l -p "${TEST_PORT}" -e bash "${BASE_DIR}/test.sh"
	sleep 1
done
